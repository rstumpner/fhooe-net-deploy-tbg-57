<!SLIDE >
# Netops
### Network Operations from Fingersyle to Semi/Automation

---
# Übersicht

---
# Librenms ( Inventory )
* Source of Truth
* Cisco APIC-EM (Cisco Prime)
---
# Oxidized (Configuration Tracking )
* Configuration Backup
* Tracking with Git
---
# Templating / Excel / Expect / Cisco Works

---
# Ansible
* Ansible Inventory
* Ansible YAMLs
* Ansible Playbooks
* Ansible Rollen
* Ansible Vault
---
# Docker
* docker Run
* Dockerfile
* Docker Repository
* docker-compose
---
# Git
* git init
* git add .
* git commit -m "First Release"
* git log --oneline
* git tag
* git status
* git brunch
* git push origin staging
* git push --tags
* git clone
---
# Gitlab
* Webinterface
* Issues
* Commits
* Merge Requests
* Developer / Master
---
# Gitlab CI/CD Pipelines and Runner
* .ci-pipeline.yml
* Gitlab Runner
* Gitlab Runner with Docker
---
# Kanboard
* Kanban Methode
* Agile
* Feature und Error Anzeige

---
# Testing / Monitoring
* Syntax
* Ansible Playbook
* Cannary Tests
* Staging ( VIRL / EVA LABS / GNS 3 / Testhardware )
* Integration Testing
* Graylog + Beats
* Ansible AWS / Tower / Foreman
---
# Rundeck + Self Service

---
# SNMP / Netconf / YOUNG

---
# NAPALM

---
# Puppet

---
# Salt

---
# Upgrade Bootup and Racking
