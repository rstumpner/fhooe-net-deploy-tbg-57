
### Network Operations from Fingersyle to ... Next Generations
---
## Agenda
* Erwartungen
* Demo
* FH-OOE Usecases
* Recap
* Future
* Übersicht
* Fragen

---
# Erwartungen
---
# Demo
---
## Demo
* Create an Issue (Gitlab)
* Create Brunch (Gitlab)
* Create a Local Copy (Clone)
* Edit your Stuff (Visual Studio Code)
* SYNC Changes (git push)
* Merge Change to Master (gitlab)

---
## Create an Issue
(Plan/ Gitlab)

![Gitlab Issue](_images/gitlab-issue-1.png)

---
## Create a Branch
(Plan/ Gitlab)

![Gitlab Issue](_images/gitlab-merge-request.png)

---
## Local Working Copy
(Plan/ Gitlab)

![Gitlab Issue](_images/git-clone-pull.png)

---
## Checkout Local Brunch
![Gitlab Issue](_images/git-checkout-brunch-terminal.png)

---
## Check Campus Variables
![Gitlab Issue](_images/vs-code-edit-campus-vars.png)

---
## Edit Device Variables
![Gitlab Issue](_images/vs-code-edit-switch-vars.png)

---
## Check Roles
![Gitlab Issue](_images/vs-code-campus-roles.png)

---
## Check Role Hostname
![Gitlab Issue](_images/vs-code-role-hostname.png)

---
## Commit Changes
![Gitlab Issue](_images/vs-code-git-commit.png)

---
## Push to Gitlab
![Gitlab Issue](_images/vs-code-push.png)

---
## CI/CD Pipeline
![Gitlab Issue](_images/gitlab-cicd-pipeline.png)

---
## Gitlab Merge
![Gitlab Issue](_images/gitlab-merge.png)

---
## Gitlab Artifacts
![Gitlab Issue](_images/gitlab-artifacts.png)

---
## Backup
![Gitlab Backup](_images/gitlab-backup.png)

---
## Backup

* Backup
* Change on Device
* Backup
---
## Testing
```YAML
# Set the SNMP Strings on Switches
# Cisco IOS and NX-OS

  - name: Switch SNMP Config
    connection: local
    tags: cisco-ios-snmp
    ios_config:
      host: "{{ ansible_ssh_host }}"
      username: "{{ ansible_ssh_user }}"
      password: "{{ ansible_ssh_password }}"
      backup: yes
      lines:
         - snmp-server community {{ snmp_community }} ro
         - snmp-server trap-source Vlan {{ net_oam_vlan }}
         - snmp-server location {{ snmp_location }}
    when:
      - not ansible_check_mode

```

---
## FH-OOE Usecases
* Network Deployment
* Network Testing
* Security
* Not only Network
---
## Recap
* Versioniert
* Nachvollziehbar
* Standartisiert
* Dokumentiert
* Fehler (ja , aber nur einmal)
---
## Future
* Network Testing
* Network Performance
* NETCONF / YANG
* Stackstorm / Chatbot

---
# Übersicht
![Toolchain Übersicht](_images/network-toolchain.png)

---
# Übersicht Pipeline
![Toolchain Übersicht](_images/cicd-execution-environment.png)

---
# Fragen
